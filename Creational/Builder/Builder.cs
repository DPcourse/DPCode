
public class Client
{
    public Computer BuildComputer(ComputerBuilder cb)
    {
        cb.assembleCPU();
        cb.assembleGPU();
        return cb.GetProduct();
    }
}

public interface ComputerBuilder
{
    void assembleCPU();
    void assembleGPU();
    Computer GetProduct();
}

public class LaptopBuilder : ComputerBuilder
{
    private Computer product = new Computer();

    public void assembleCPU()
    {
        product.CPU = "Laptop CPU";
    }

    public void assembleGPU()
    {
        product.GPU = "Laptop GPU";
    }

    public Computer GetProduct()
    {
        return product;
    }
}

public class DesktopBuilder : ComputerBuilder
{
    private Computer product = new Computer();

    public void assembleCPU()
    {
        product.CPU = "Desktop CPU";
    }

    public void assembleGPU()
    {
        product.GPU = "Desktop GPU";
    }

    public Computer GetProduct()
    {
        return product;
    }
}

public class Airplane
{
    public string CPU;
    public string GPU;

}