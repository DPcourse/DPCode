public abstract class Cell
{
    // normal implementation
    public abstract Cell Clone();
}

public class Prokaryote : Cell
{
    public override Cell Clone()
    {
        return (Cell)this.MemberwiseClone(); // Clones the concrete class.
    }
}

public class Eukaryote : Cell
{
    public override Cell Clone()
    {
        return (Cell)this.MemberwiseClone(); // Clones the concrete class.
    }
}