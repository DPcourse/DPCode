class CarFactory
{
    IButton CreateButton();
}

class IEngine
{
	
}

class IWheel
{
	
}

class EuropeanFactory : CarFactory
{
    public IEngine CreateEngine()
    {
        return new EuropeanEngine();
    }
}

class AmericanFactory : CarFactory
{
    public IEngine CreateEngine()
    {
        return new AmericanEngine();
    }
}

class EuropeanEngine : IEngine
{

}

class AmericanEngine : IEngine
{

}

class EuropeanWheel : IWheel
{

}

class AmericanWheel : IWheel
{

}
